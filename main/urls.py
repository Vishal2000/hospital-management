from django.urls import path
from . import views
from .views import autosuggest, json_bed_edit

urlpatterns = [
    # path("", views.index, name="index"),
    path("", views.dashboard, name="dashboard"),
    path("dashboard2/", views.dashboard2, name="dashboard2"),
    path("base/", views.base, name="base"),    
    path("login/", views.login, name="login"),
    path("forgot_password/", views.forgot_password, name="forgot_password"),
    path('logout', views.logout, name='logout'),
    path("add_patient/", views.add_patient, name="add_patient"),
    path("add_patient_admin/", views.add_patient_admin, name="add_patient_admin"),
    path("patient_list/", views.patient_list, name="patient_list"),
    path("patient_list_admin/", views.patient_list_admin, name="patient_list_admin"),
    path("patient_list/patient/<pk>/", views.patient, name="patient"),
    path("patient_list_admin/patient/<pk>/", views.patient_admin, name="patient_admin"),
    path("autosuggest/", views.autosuggest, name="autosuggest"),
    path("autodoctor/", views.autodoctor, name="autodoctor"),
    path("autobed/", views.autobed, name="autobed"),
    path("info/", views.info, name="info"),
    path("add_bed/", views.add_bed, name="add_bed"),
    path("add_bed_admin/", views.add_bed_admin, name="add_bed_admin"),
    path("view_bed/", views.view_bed, name="view_bed"),
    path("view_bed_admin/", views.view_bed_admin, name="view_bed_admin"),
    path("view_bed/update_bed/<pk>/", views.update_bed, name="update_bed"),
    path("view_bed_admin/update_bed/<pk>/", views.update_bed_admin, name="update_bed_admin"),
    path("add_user/", views.add_user, name="add_user"),
    path("user_list/", views.user_list, name="user_list"),
    path("user_list/user_update/<pk>/", views.user_update, name="user_update"),
    path('add_doctor/',views.add_doctor,name="add_doctor"),
    path('view_doctor/',views.view_doctor,name="view_doctor"),
    path('view_doctor/update_doctor/<pk>/',views.update_doctor,name="update_doctor"),
    path('view_ur_patient/doctor_edit_patient/<pk>/',views.doctor_edit_patient,name="doctor_edit_patient"),
    path('bed_master_data/',views.bed_master.as_view()), 
    path('patient_master_data/',views.patient_master.as_view()), 
    path('user_master_data/',views.user_master.as_view()), 
    path('doctor_master_data/',views.doctor_master.as_view()), 
    path('doctor_patient_master/',views.doctor_patient_master.as_view()), 
    path('edit_profile/',views.edit_profile,name="edit_profile"), 
    path('edit_profile_admin/',views.edit_profile_admin,name="edit_profile_admin"), 
    path('view_ur_patient/',views.view_ur_patient,name="view_ur_patient"), 
    path('edit_profile_doctor/',views.edit_profile_doctor,name="edit_profile_doctor"), 


    path('patient_list_admin/delete_patient/<pk>/',views.delete_patient,name="delete_patient"), 
    path('view_bed_admin/delete_bed/<pk>/',views.delete_bed,name="delete_bed"), 
    path('user_list/delete_user/<pk>/',views.delete_user,name="delete_user"), 
    path('view_doctor/delete_doctor/<pk>/',views.delete_doctor,name="delete_doctor"), 

    path('json_bed/',views.json_bed,name="json_bed"),
    path('json_bed_edit/<pk>/',views.json_bed_edit,name="json_bed_edit")


]
