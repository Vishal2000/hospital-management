from email.mime import image
from email.policy import default
from operator import mod
from unicodedata import category
from django.db import models
from multiselectfield import MultiSelectField
from django.contrib.auth.models import User

# Create your models here.

class Patient(models.Model):
    name = models.CharField(max_length=50)
    phone_num = models.CharField(max_length=15)
    patient_relative_name = models.CharField(max_length=50, null=True)
    patient_relative_contact = models.CharField(max_length=15, null=True)
    gender_choices = (('M', 'Male'), ('F', 'Female'),)
    gender = models.CharField(max_length=6, choices=gender_choices)
    address = models.CharField(max_length=100, null=True)
    SYMPTOMS = (
        ('Fever', 'Fever'),
        ('Dry cough', 'Dry cough'),
        ('Tiredness', 'Tiredness'),
    )

    comman_symptoms = MultiSelectField(choices=SYMPTOMS,null=True,blank=True,max_length=200)
    SYMPTOMS2 = (
        
        ('Aches and pains', 'Aches and pains'),
        ('Sore throat', 'Sore throat'),
        ('Diarrhoea', 'Diarrhoea'),
        ('Loss of taste or smell', 'Loss of taste or smell'),
    )
    less_symptoms = MultiSelectField(choices=SYMPTOMS2, null=True,blank=True,max_length=200)
    SYMPTOMS3 = (
        ('Difficulty in breathing or shortness of breath', 'Difficulty in breathing or shortness of breath'),
        ('Chest pain or pressure', 'Chest pain or pressure'),
        ('Loss of speech or movement', 'Loss of speech or movement'),
    )
    serious_symptoms =  MultiSelectField(choices=SYMPTOMS3, null=True,blank=True,max_length=200)
    prior_ailments = models.TextField()
    bed_num = models.ForeignKey("Bed", on_delete=models.CASCADE,null=True)
    dob = models.DateField(null=True)
    doctor = models.ForeignKey("Doctor", on_delete=models.CASCADE, null=True)
    status = models.CharField(max_length=50)
    
    def __str__(self):
        return self.name
        
class Bed(models.Model):
    yes = 'Yes'
    no = 'No'
    TYPE_CHOICES = (
        (yes, 'Yes'),
        (no, 'No')
    )
    simple = 'Simple'
    ventilator = 'Ventilator'
    BED_TYPE = (
        (simple ,'Simple'),
        (ventilator , 'Ventilator')
    )
    bed_number = models.CharField(max_length=50)
    occupied = models.CharField(max_length=10, choices=TYPE_CHOICES, default=no)
    categories = models.CharField(max_length=10, choices = BED_TYPE, default=simple)
    def __str__(self):
        return self.bed_number


class Doctor(models.Model):
    user_id = models.ForeignKey(User, on_delete=models.CASCADE, null=True)
    name = models.CharField(max_length=100)
    phone_num = models.CharField(max_length=15)
    gender_choices = (('M', 'Male'), ('F', 'Female'),)
    gender = models.CharField(max_length=6, choices=gender_choices)
    dob = models.DateField(null=True)
    email = models.EmailField()
    address = models.TextField()
    state = models.CharField(max_length=50)
    city = models.CharField(max_length=50)
    specialization = models.CharField(max_length=150)

    def __str__(self):
        return self.name

class Add_user(models.Model):
    user_id = models.ForeignKey(User, on_delete=models.CASCADE, null=True)
    name = models.CharField(max_length=50)
    phone_num = models.CharField(max_length=15)
    gender_choices = (('M', 'Male'), ('F', 'Female'),)
    gender = models.CharField(max_length=6, choices=gender_choices)
    dob = models.DateField(null=True)
    email = models.EmailField()
    address = models.TextField()
    state = models.CharField(max_length=50)
    city = models.CharField(max_length=50)
    password = models.CharField(max_length=9)
    def __str__(self):
        return self.name

class Profile(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True)
    location =  models.ImageField(upload_to='images', default='images/visitor.png')

