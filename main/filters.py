# import django.contrib.admin.filters
from dataclasses import fields
import django
import django_filters
from .models import *

# ModelnameFilter
class PatientFilter(django_filters.FilterSet):
    class Meta:
        model = Patient
        fields = ['name', 'bed_num', 'doctor','status']
        # fields = '__all__'
        # exclude = ['phone_num', 'address', 'prior_ailments', 'dob', 'patient_relative_name', 'patient_relative_contact', 'symptoms', 'doctors_visiting_time', 'doctors_notes']
        
class BedFilter(django_filters.FilterSet):
    class Meta:
        model = Bed
        fields = '__all__'

        
class UserFilter(django_filters.FilterSet):
    class Meta:
        model = Add_user
        fields = ['name','email']
        exclude = ['phone_num', 'address','dob']

class DoctorFilter(django_filters.FilterSet):
    class Meta:
        model = Doctor
        fields = ['name','specialization']
       