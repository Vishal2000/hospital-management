from django.contrib import admin
from .models import *
# Register your models here.
admin.site.register(Patient)
admin.site.register(Bed)
admin.site.register(Doctor)
admin.site.register(Add_user)
admin.site.register(Profile)