import datetime
from django.shortcuts import render, redirect
from django.http import HttpResponse, JsonResponse
from .models import *
from .filters import PatientFilter,BedFilter,UserFilter,DoctorFilter
from django.contrib.auth.models import User, auth
from django.contrib import messages
from django.contrib.auth.decorators import login_required
import string
from django.core.mail import EmailMessage
from random import *
from django.template.loader import render_to_string
from django.core.files.base import ContentFile
import base64
import time
import json
from django.db.models import Q
from django.core.serializers.json import DjangoJSONEncoder
from django_serverside_datatable.views import ServerSideDatatableView
import validators
# PatientFilter = OrderFilter

# Create your view

def base(request):
    # user = request.user
    # user_id = user.id
    # print('------user----',user)
    # profile_admin = Profile.objects.get(user=user_id)
    # print('----admin---',profile_admin)
    # context = {
    #     'profile_admin' : profile_admin ,
    # }
    # user = request.user
    # doctor_user = Doctor.objects.filter(user_id=user)
    # print("-----doctor-----",doctor_user)
    # show = False
    # if doctor_user.exists():
    #     show = True   
    request(request,'main/base.html')



def login(request):
    if not request.user.is_authenticated:
        if request.method == 'POST':
            error = {}

            username = request.POST['username']
            password = request.POST['password']
 
            if not validators.length(username, min=2, max=100):
                error['username'] = "Please enter username"
                print('-----------',error)
            if not validators.length(password, min=1):
                error['password'] = "Please enter password"
            if bool(error):
                return JsonResponse({'success': 'false', 'error': error}, status=422)

            user = auth.authenticate(username=username, password=password)

            if 'is_remember_check' in request.POST:
                request.session['username'] = request.POST.get('username')
                request.session['password'] = request.POST.get('password')

            doctor_user = Doctor.objects.filter(user_id=user)
            print("-----doctor-----",doctor_user)
            show = False
            if doctor_user.exists():
                show = True  

            if user is not None and show == False:
                auth.login(request, user)
                request.session['id'] = user.id
               
                response = JsonResponse(
                    {'success': True, 'message': 'Login successful!', 'url': 'dashboard'}, status=200)
                response.set_cookie(key='sa-username',
                                    value=request.POST.get('username'))
                response.set_cookie(key='sa-password',
                                    value=request.POST.get('password'))
                return response
            elif user is not None and show == True:
                auth.login(request, user)
                request.session['id'] = user.id
               
                response = JsonResponse(
                    {'success': True, 'message': 'Login successful!', 'url': 'dashboard2'}, status=200)
                response.set_cookie(key='sa-username',
                                    value=request.POST.get('username'))
                response.set_cookie(key='sa-password',
                                    value=request.POST.get('password'))
                return response
            else:
                    error['password'] = "Credential invalid"
                    return JsonResponse({'success': False, 'error': error}, status=422)

        if request.COOKIES.get('sa-username') and request.COOKIES.get('sa-password'):
            username = request.COOKIES['sa-username']
            password = request.COOKIES['sa-password']
            context_dict = {'username': username, 'password': password}
            return render(request, 'main/login.html', context_dict)
        else:
            context_dict = {'username': '', 'password': ''}
            return render(request, 'main/login.html', context_dict)
        # else:
        #     return render(request, 'main/login.html')
    else:
        return redirect('dashboard')

@login_required(login_url='login')
def logout(request):
    auth.logout(request)
    return redirect('/')

def forgot_password(request):
    if request.method == 'POST':
        error = {}

        email = request.POST['email']
        if not validators.length(email, min=2, max=100):
                error['email'] = "Please enter email id"
        elif not validators.email(email):
                error['email'] = "Please enter valid email id"

        if bool(error):
                return JsonResponse({'success': 'false', 'error': error}, status=422)
                
        user = User.objects.filter(email=email)
        show = False
        print('email',email)
        if user.exists():
            show = True
        print('user',user)
        if show == True :
                user = User.objects.get(email=email)
                characters = string.ascii_letters + string.digits
                password = "".join(choice(characters)for x in range(randint(8, 8)))
                user.set_password(password)
                user.save()

                username = user.username

                un = 'Username: ' + username + '\n'
                pas = 'Password: ' + password + '\n'

                send = un + pas
                em = EmailMessage('Username and Password is send',send,'patelbhumin9177@gmail.com',to=[email])
                em.send()
        else:
            error['email'] = "Email does not exists"
            return JsonResponse({'success': 'false', 'error': error}, status=422)

    return render(request,'main/forgot_password.html')




# def reset_password(request):
#     if request.method == 'POST':
#         error = {}

#         password1 = request.POST['password1']
#         password2 = request.POST['password2']
#         if not validators.length(password1, min=8, max=100):
#                 error['password1'] = "Please enter maximum 8 characters"
#         if not validators.length(password2, min=8, max=100):
#                 error['password2'] = "Please enter maximum 8 characters"
#         if bool(error):
#                 return JsonResponse({'success': 'false', 'error': error}, status=422)

#         if password1 != password2:
#             error['password2'] = 'Password did not match'
#             if bool(error):
#                 return JsonResponse({'success': 'false', 'error': error}, status=422)

#         user = User.objects.get
#     return render(request,'main/reset_password.html')




def dashboard(request):
    user = request.user
    user_id = user.id
    pic = Profile.objects.filter(user=user_id)
    # print('----pic---',pics)
    show = False
    if pic.exists() :
        show = True
    print('------show------',show)
    patients = Patient.objects.all()
    patient_count = patients.count()
    patients_recovered = Patient.objects.filter(status="Recovered")
    patients_deceased = Patient.objects.filter(status="Deceased")
    patients_recovering = Patient.objects.filter(status="Recovering")
    deceased_count = patients_deceased.count()
    recovered_count = patients_recovered.count()
    recovering_count = patients_recovering.count()
    beds = Bed.objects.all()
    beds_available = Bed.objects.filter(occupied='No').count()
    beds_occcupied = Bed.objects.filter(occupied='Yes').count()

    doctor = Doctor.objects.all().count()
    print("----------",doctor)
    context = {
        'patient_count': patient_count,
        'recovered_count': recovered_count,
        'beds_available': beds_available,
        'deceased_count':deceased_count,
        'recovering_count':recovering_count,
        'beds':beds,
        'doctor': doctor,
        'pic' : pic,
        'show' : show,
        'beds_occcupied' : beds_occcupied,  
    }
    print(patient_count)
    return render(request, 'main/dashboard.html', context)

def dashboard2(request):
    user = request.user
    user_id = user.id
    pic = Profile.objects.filter(user=user_id)
    print('----pic---',pic)
    show = False
    if pic.exists() :
        show = True
    print('------show------',show)
    patients = Patient.objects.all()
    patient_count = patients.count()
    patients_recovered = Patient.objects.filter(status="Recovered")
    patients_deceased = Patient.objects.filter(status="Deceased")
    patients_recovering = Patient.objects.filter(status="Recovering")
    deceased_count = patients_deceased.count()
    recovered_count = patients_recovered.count()
    recovering_count = patients_recovering.count()
    beds = Bed.objects.all()
    beds_available = Bed.objects.filter(occupied='No').count()
    beds_occcupied = Bed.objects.filter(occupied='Yes').count()

    doctor = Doctor.objects.all().count()
    print("----------",doctor)
    context = {
        'patient_count': patient_count,
        'recovered_count': recovered_count,
        'beds_available': beds_available,
        'deceased_count':deceased_count,
        'recovering_count':recovering_count,
        'beds':beds,
        'doctor': doctor,
        'pic' : pic,
        'show' : show,
        'beds_occcupied' : beds_occcupied,  
    }
    print(patient_count)
    return render(request, 'main/dashboard2.html', context)

# def dashboard2(request):
#     patients = Patient.objects.all()
#     patient_count = patients.count()
#     patients_recovered = Patient.objects.filter(status="Recovered")
#     patients_deceased = Patient.objects.filter(status="Deceased")
#     patients_recovering = Patient.objects.filter(status="Recovering")
#     deceased_count = patients_deceased.count()
#     recovered_count = patients_recovered.count()
#     recovering_count = patients_recovering.count()
#     beds = Bed.objects.all()
#     beds_available = Bed.objects.filter(occupied='No').count()
#     doctor = Doctor.objects.all().count()
#     print("----------",doctor)
#     context = {
#         'patient_count': patient_count,
#         'recovered_count': recovered_count,
#         'beds_available': beds_available,
#         'deceased_count':deceased_count,
#         'recovering_count':recovering_count,
#         'beds':beds,
#         'doctor': doctor,
#         'show' : show,

#     }
    # print(patient_count)
    # return render(request, 'main/dashboard2.html', context)
    

def add_patient(request):
    user = request.user
    user_id = user.id
    pic = Profile.objects.filter(user=user_id)
    # pics = pic.values_list('location', flat = True)
    # print('----pic---',pics)
    show = False
    if pic.exists() :
        show = True
    print('------show------',show)
    beds = Bed.objects.filter(occupied='No')
    doctors = Doctor.objects.all()
    if request.method == "POST":
        error = {}
        name = request.POST['name']
        phone_num = request.POST['phone_num']
        patient_relative_name = request.POST['patient_relative_name']
        patient_relative_contact = request.POST['patient_relative_contact']
        gender = request.POST['gender']
        address = request.POST['address']
        prior_ailments = request.POST['prior_ailments']
        bed_num_sent = request.POST['bed_num']
        print('---------------',len(bed_num_sent))
        dob = request.POST['dob']
        status = request.POST['status']
        doctor = request.POST['doctor']
        comman_symptoms = request.POST.getlist('symptoms')
        less_symptoms = request.POST.getlist('symptoms2')
        serious_symptoms = request.POST.getlist('symptoms3')
        print(request.POST)

        if not validators.length(name, min=2, max=100):
                error['name'] = "Please enter valid name"
        if not validators.length(phone_num, max=10, min=10) or not phone_num.isdigit():
                error['phone_num'] = "Mobile number is not valid"
        if not validators.length(patient_relative_name, min=2, max=100):
                error['patient_relative_name'] = "Please enter relative name"
        if not validators.length(patient_relative_contact, max=10, min=10) or not patient_relative_contact.isdigit():
                error['patient_relative_contact'] = "Mobile number is not valid"
        if len(address) == 0:
                error['address'] = "Address is required"
        if not validators.length(prior_ailments, min=2, max=100):
                error['prior_ailments'] = "Please enter prior ailments"
        if len(request.POST['bed_num']) == 0:
                error['bed_num'] = "Bed number is required"
        if len(dob) == 0:
                error['dob'] = "Date is required"
        if len(status) == 0:
                error['status'] = "Status is required"
        if len(request.POST['doctor']) == 0:
                error['doctor'] = "Doctor is required"

        if bool(error):
                return JsonResponse({'success': 'false', 'error': error}, status=422)        
        bed_num = Bed.objects.get(bed_number=bed_num_sent)
        doctor = Doctor.objects.get(name=doctor)
        patient = Patient.objects.create(
        name = name,
        phone_num = phone_num,
        patient_relative_name = patient_relative_name,
        patient_relative_contact = patient_relative_contact, 
        gender=gender,  
        address = address, 
        comman_symptoms = comman_symptoms, 
        less_symptoms = less_symptoms, 
        serious_symptoms = serious_symptoms, 
        prior_ailments = prior_ailments, 
        bed_num = bed_num,
        dob = dob, 
        doctor=doctor,
        status = status
        )
        patient.save()
        bed = Bed.objects.get(bed_number=bed_num_sent)
        bed.occupied = 'Yes'
        bed.save()
        id = patient.id
        # return redirect(f"/patient/{id}")
        return JsonResponse({'success': True, 'message': 'Successfully created', 'url': '/add_patient/'}, status=200)

    context = {
        'beds': beds,
        'doctors': doctors,
        'pic' : pic,
        'show' : show,
    }
    
    return render(request, 'main/add_patient.html', context)

def add_patient_admin(request):
    user = request.user
    user_id = user.id
    pic = Profile.objects.filter(user=user_id)
    # pics = pic.values_list('location', flat = True)
    # print('----pic---',pics)
    show = False
    if pic.exists() :
        show = True
    beds = Bed.objects.filter(occupied='No')
    get_beds = Bed.objects.filter(occupied='No').values('id','bed_number','occupied','categories')
    print('------show------',get_beds)
    doctors = Doctor.objects.all()
    # post_list = serializers.serialize('json',
    #                                   list(beds))
    post_list = json.dumps(list(get_beds), cls=DjangoJSONEncoder)
    if request.method == "POST":
        error = {}
        name = request.POST['name']
        phone_num = request.POST['phone_num']
        patient_relative_name = request.POST['patient_relative_name']
        patient_relative_contact = request.POST['patient_relative_contact']
        gender = request.POST['gender']
        address = request.POST['address']
        prior_ailments = request.POST['prior_ailments']
        doctor = request.POST['doctor']
        dob = request.POST['dob']
        status = request.POST['status']
        bed_num_sent = request.POST['bed_num']
        print('---------------',len(bed_num_sent))
        comman_symptoms = request.POST.getlist('symptoms')
        less_symptoms = request.POST.getlist('symptoms2')
        serious_symptoms = request.POST.getlist('symptoms3')
        print(request.POST)

        if not validators.length(name, min=2, max=100):
                error['name'] = "Please enter valid name"
        if not validators.length(phone_num, max=10, min=10) or not phone_num.isdigit():
                error['phone_num'] = "Mobile number is not valid"
        if not validators.length(patient_relative_name, min=2, max=100):
                error['patient_relative_name'] = "Please enter relative name"
        if not validators.length(patient_relative_contact, max=10, min=10) or not patient_relative_contact.isdigit():
                error['patient_relative_contact'] = "Mobile number is not valid"
        if len(address) == 0:
                error['address'] = "Address is required"
        if not validators.length(prior_ailments, min=2, max=100):
                error['prior_ailments'] = "Please enter prior ailments"
        if len(request.POST['bed_num']) == 0:
                error['bed_num'] = "Bed number is required"
        if len(dob) == 0:
                error['dob'] = "Date is required"
        if len(status) == 0:
                error['status'] = "Status is required"
        if len(request.POST['doctor']) == 0:
                error['doctor'] = "Doctor is required"

        if bool(error):
                return JsonResponse({'success': 'false', 'error': error}, status=422)        
        bed_num = Bed.objects.get(bed_number=bed_num_sent)
        doctor = Doctor.objects.get(name=doctor)
        patient = Patient.objects.create(
        name = name,
        phone_num = phone_num,
        patient_relative_name = patient_relative_name,
        patient_relative_contact = patient_relative_contact, 
        gender=gender,  
        address = address, 
        comman_symptoms = comman_symptoms, 
        less_symptoms = less_symptoms, 
        serious_symptoms = serious_symptoms, 
        prior_ailments = prior_ailments, 
        bed_num = bed_num,
        dob = dob, 
        doctor=doctor,
        status = status
        )
        patient.save()
        bed = Bed.objects.get(bed_number=bed_num_sent)
        bed.occupied = 'Yes'
        bed.save()
        id = patient.id
        # return redirect(f"/patient/{id}")
        return JsonResponse({'success': True, 'message': 'Successfully created', 'url': '/add_patient/'}, status=200)

    context = {
        'beds': beds,
        'doctors': doctors,
        'pic' : pic,
        'show' : show,
        'get_beds': get_beds,
        'post_list' : post_list,
    }
    print("context:",context)
    return render(request, 'main/add_patient_admin.html', context)

def json_bed(request):
    get_beds = Bed.objects.filter(occupied='No').values('id','bed_number','occupied','categories')
    post_list = json.dumps(list(get_beds), cls=DjangoJSONEncoder)
    return JsonResponse(post_list,safe=False)

def json_bed_edit(request,pk):
    patient = Patient.objects.get(id=pk)
    # get_patient = Patient.objects.filter(id=pk).values('id')
    # post_patient = json.dumps(list(get_patient), cls=DjangoJSONEncoder)
    get_beds = Bed.objects.filter(occupied='No').values('id','bed_number','occupied','categories')
    print('------getbed----',get_beds)
    post_list = json.dumps(list(get_beds), cls=DjangoJSONEncoder)
    return JsonResponse(post_list,safe=False)

def patient(request, pk):
    user = request.user
    user_id = user.id
    pic = Profile.objects.filter(user=user_id)
    # print('----pic---',pics)
    show = False
    if pic.exists() :
        show = True
    print('------show------',show)
    beds = Bed.objects.filter(occupied='No')
    patient = Patient.objects.get(id=pk)
    doctors = Doctor.objects.all()
    old = patient.bed_num_id

    if request.method == "POST":
        
        name = request.POST['name']
        mobile = request.POST['phone_num']
        mobile2 = request.POST['mobile2']
        relativeName = request.POST['relativeName']
        gender = request.POST['gender']
        address  = request.POST['location']
        prior_ailments = request.POST['prior_ailments']
        status = request.POST['status']
        bed_num_sent = request.POST.get('bed_num')
        bed_num = Bed.objects.filter(bed_number=bed_num_sent)
        dob = request.POST['dob']
        doctor = request.POST['doctor']
        doctor = Doctor.objects.get(name=doctor)
        comman_symptoms = request.POST.getlist('symptoms')
        less_symptoms = request.POST.getlist('symptoms2')
        serious_symptoms = request.POST.getlist('symptoms3')

        error = {}
        if not validators.length(request.POST['name'], min=2, max=100):
                error['name'] = "Please enter valid name"
        if not validators.length(mobile, max=10, min=10) or not mobile.isdigit():
                error['phone_num'] = "Mobile number is not valid"
        if not validators.length(relativeName, min=2, max=100):
                error['relativeName'] = "Please enter relative name"
        if not validators.length(mobile2, max=10, min=10) or not mobile2.isdigit():
                error['mobile2'] = "Mobile number is not valid"
        if len(address) == 0:
                error['location'] = "Address is required"
        if not validators.length(prior_ailments, min=2, max=100):
                error['prior_ailments'] = "Please enter prior ailments"
        # if len(request.POST['bed_num']) == 0:
        #         error['bed_num'] = "Bed number is required"
        if len(dob) == 0:
                error['dob'] = "Date is required"
        if len(status) == 0:
                error['status'] = "Status is required"
        if len(request.POST['doctor']) == 0:
                error['doctor'] = "Doctor is required"

        if bool(error):
                return JsonResponse({'success': 'false', 'error': error}, status=422)        

        patient.name = name
        if bed_num.exists():        
            patient.bed_num = bed_num
        else:
            patient.bed_num = None
        patient.phone_num = mobile
        patient.patient_relative_contact = mobile2
        patient.patient_relative_name = relativeName
        patient.gender=gender
        patient.address = address
        patient.prior_ailments = prior_ailments
        patient.dob = dob
        patient.doctor = doctor
        patient.status = status
        patient.comman_symptoms = comman_symptoms
        patient.less_symptoms = less_symptoms
        patient.serious_symptoms = serious_symptoms
        print('----------------bed_num--------------',patient.bed_num)
        patient.save()
        print('----------------bed_num--------------',patient.bed_num)

        bed = Bed.objects.filter(id=patient.bed_num_id)
        print('----------------bed_num--------------',bed)
        if bed.exists():
            bed = Bed.objects.get(id=patient.bed_num_id)
            bed.occupied = 'Yes' 
            if patient.status == 'Recoverd':
                bed.occupied = 'No'
            bed.save()
        beds = Bed.objects.filter(id=old)
        print('----------------bed_numsssss--------------',beds)
        if beds.exists():
            beds = Bed.objects.get(id=old)
            beds.occupied = 'No' 
            beds.save()
       
        # return redirect(f"/patient/{id}")
        return JsonResponse({'success': True, 'message': 'Successfully created', 'url': '/patient_list/'}, status=200)

    context = {
        'patient': patient,
        'beds' : beds,
        'doctors' : doctors,
        'pic' : pic,
        'show' : show,

    }
    return render(request, 'main/patient.html', context)

def patient_admin(request, pk):
    user = request.user
    user_id = user.id
    pic = Profile.objects.filter(user=user_id)
    # print('----pic---',pics)
    show = False
    if pic.exists() :
        show = True
    print('------show------',show)
    beds = Bed.objects.filter(occupied='No')
    patient = Patient.objects.get(id=pk)
    doctors = Doctor.objects.all()
    old = patient.bed_num_id

    if request.method == "POST":
        
        name = request.POST['name']
        mobile = request.POST['phone_num']
        mobile2 = request.POST['mobile2']
        relativeName = request.POST['relativeName']
        gender = request.POST['gender']
        address  = request.POST['location']
        prior_ailments = request.POST['prior_ailments']
        status = request.POST['status']
        bed_num_sent  = request.POST.get('bed_num')
        print('------ggg---', bed_num_sent)
        bed_num = Bed.objects.filter(bed_number=bed_num_sent)
        dob = request.POST['dob']
        doctor = request.POST['doctor']
        doctor = Doctor.objects.get(name=doctor)
        comman_symptoms = request.POST.getlist('symptoms')
        less_symptoms = request.POST.getlist('symptoms2')
        serious_symptoms = request.POST.getlist('symptoms3')

        error = {}
        if not validators.length(request.POST['name'], min=2, max=100):
                error['name'] = "Please enter valid name"
        if not validators.length(mobile, max=10, min=10) or not mobile.isdigit():
                error['phone_num'] = "Mobile number is not valid"
        if not validators.length(relativeName, min=2, max=100):
                error['relativeName'] = "Please enter relative name"
        if not validators.length(mobile2, max=10, min=10) or not mobile2.isdigit():
                error['mobile2'] = "Mobile number is not valid"
        if len(address) == 0:
                error['location'] = "Address is required"
        if not validators.length(prior_ailments, min=2, max=100):
                error['prior_ailments'] = "Please enter prior ailments"
        if len(request.POST['bed_num']) == 0:
                error['bed_num'] = "Bed number is required"
        if len(dob) == 0:
                error['dob'] = "Date is required"
        if len(status) == 0:
                error['status'] = "Status is required"
        if len(request.POST['doctor']) == 0:
                error['doctor'] = "Doctor is required"

        if bool(error):
                return JsonResponse({'success': 'false', 'error': error}, status=422)        

        patient.name = name
        patient.bed_num = bed_num
        patient.phone_num = mobile
        patient.patient_relative_contact = mobile2
        patient.patient_relative_name = relativeName
        patient.gender=gender
        patient.address = address
        patient.prior_ailments = prior_ailments
        patient.dob = dob
        patient.doctor = doctor
        patient.status = status
        patient.comman_symptoms = comman_symptoms
        patient.less_symptoms = less_symptoms
        patient.serious_symptoms = serious_symptoms
        print('----------------bed_num--------------',patient.bed_num)
        patient.save()
        print('----------------bed_num--------------',patient.bed_num)

        bed = Bed.objects.get(id=patient.bed_num_id)
        print('----------------bed_num--------------',bed)
        bed.occupied = 'Yes' 
        if patient.status == 'Recoverd':
            bed.occupied = 'No'
        bed.save()
        beds = Bed.objects.get(id=old)
        print('----------------bed_numsssss--------------',beds)
        beds.occupied = 'No' 
        beds.save()
        id = patient.id
        # return redirect(f"/patient/{id}")
        return JsonResponse({'success': True, 'message': 'Successfully created', 'url': '/patient_list_admin/'}, status=200)

    context = {
        'patient': patient,
        'beds' : beds,
        'doctors' : doctors,
        'pic' : pic,
        'show' : show,

    }
    return render(request, 'main/patient_admin.html', context)


def patient_list(request):
    user = request.user
    user_id = user.id
    pic = Profile.objects.filter(user=user_id)
    # print('----pic---',pics)
    show = False
    if pic.exists() :
        show = True
    print('------show------',show)
    patients = Patient.objects.all()

#     # filtering
#     myFilter = PatientFilter(request.GET, queryset=patients)

#     patients = myFilter.qs
    context = {
        'patients': patients,
        'pic' : pic,
        'show' : show,

        # 'myFilter': myFilter
    }

    return render(request, 'main/patient_list.html', context)

def patient_list_admin(request):
    user = request.user
    user_id = user.id
    pic = Profile.objects.filter(user=user_id)
    # print('----pic---',pics)
    show = False
    if pic.exists() :
        show = True
    print('------show------',show)
    patients = Patient.objects.all()

#     # filtering
#     myFilter = PatientFilter(request.GET, queryset=patients)

#     patients = myFilter.qs
    context = {
        'patients': patients,
        'pic' : pic,
        'show' : show,

        # 'myFilter': myFilter
    }

    return render(request, 'main/patient_list_admin.html', context)

def delete_patient(request,pk):
        pi = Patient.objects.get(id=pk)
        print('-----pi----',pi)
        pi_bed = Bed.objects.get(id=pi.bed_num_id)
        print('-----pi_bed----',pi_bed)
        pi_bed.occupied = 'No'
        print('-----pi_bed_occ----',pi_bed.occupied)   
        pi_bed.save()     
        pi.delete()
        return redirect('/patient_list_admin/')




class patient_master(ServerSideDatatableView):
    table_name = 'patient_master'
    queryset = Patient.objects.all()
    columns = ['name', 'bed_num__bed_number', 'status','doctor__name','id']


'''
def autocomplete(request):
    if patient in request.GET:
        name = Patient.objects.filter(name__icontains=request.GET.get(patient))
        name = ['js', 'python']
        
        names = list()
        names.append('Shyren')
        print(names)
        for patient_name in name:
            names.append(patient_name.name)
        return JsonResponse(names, safe=False)
    return render (request, 'main/patient_list.html')
'''

def autosuggest(request):
    query_original = request.GET.get('term')
    queryset = Patient.objects.filter(name__icontains=query_original)
    mylist = []
    mylist += [x.name for x in queryset]
    return JsonResponse(mylist, safe=False)

def autodoctor(request):
    query_original = request.GET.get('term')
    queryset = Doctor.objects.filter(name__icontains=query_original)
    mylist = []
    mylist += [x.name for x in queryset]
    return JsonResponse(mylist, safe=False)


def info(request):
    user = request.user
    user_id = user.id
    doctor = Doctor.objects.filter(user_id=user_id)
    show_doctor = False
    if doctor.exists():
        show_doctor = True
    return render(request, "main/info.html",{'show_doctor' : show_doctor,})

def add_bed(request):
    user = request.user
    user_id = user.id
    pic = Profile.objects.filter(user=user_id)
    # print('----pic---',pics)
    show = False
    if pic.exists() :
        show = True
    print('------show------',show)
    if request.method == "POST":
        error = {}
        bed_number = request.POST.get('bed_number')
        occupied = request.POST.get('occupied')

        if not validators.length(bed_number, min=2, max=100):
                error['bed_number'] = "Please enter bed number"

        if bool(error):
                return JsonResponse({'success': 'false', 'error': error}, status=422)  

        bed = Bed.objects.create(

            bed_number = bed_number,
            occupied = occupied,
        )
    
        bed.save()
        return JsonResponse({'success': True, 'message': 'Successfully created', 'url': '/view_bed/'}, status=200)

    return render(request,'main/add_bed.html',{'pic' : pic,'show' : show,})

def add_bed_admin(request):
    user = request.user
    user_id = user.id
    pic = Profile.objects.filter(user=user_id)
    # print('----pic---',pics)
    show = False
    if pic.exists() :
        show = True
    print('------show------',show)
    if request.method == "POST":
        error = {}
        bed_number = request.POST.get('bed_number')
        occupied = request.POST.get('occupied')

        if not validators.length(bed_number, min=2, max=100):
                error['bed_number'] = "Please enter bed number"

        if bool(error):
                return JsonResponse({'success': 'false', 'error': error}, status=422)  

        bed = Bed.objects.create(

            bed_number = bed_number,
            occupied = occupied,
        )
    
        bed.save()
        return JsonResponse({'success': True, 'message': 'Successfully created', 'url': '/view_bed/'}, status=200)

    return render(request,'main/add_bed_admin.html',{'pic' : pic,'show' : show,})

class bed_master(ServerSideDatatableView):
    table_name = 'bed_master'
    queryset = Bed.objects.all()
    columns = ['bed_number', 'occupied', 'id']

def view_bed(request):
    user = request.user
    user_id = user.id
    pic = Profile.objects.filter(user=user_id)
    # print('----pic---',pics)
    show = False
    if pic.exists() :
        show = True
    print('------show------',show)
    beds = Bed.objects.all()
    
    # filtering
#     myFilter = BedFilter(request.GET, queryset=beds)

#     beds = myFilter.qs
#     print('---bed----',beds)
    context = {
        'beds': beds,
        'pic' : pic,
        'show' : show,

        # 'myFilter': myFilter,
    }
    return render(request,'main/view_bed.html',context)

def view_bed_admin(request):
    user = request.user
    user_id = user.id
    pic = Profile.objects.filter(user=user_id)
    # print('----pic---',pics)
    show = False
    if pic.exists() :
        show = True
    print('------show------',show)
    beds = Bed.objects.all()
    
    # filtering
#     myFilter = BedFilter(request.GET, queryset=beds)

#     beds = myFilter.qs
#     print('---bed----',beds)
    context = {
        'beds': beds,
        'pic' : pic,
        'show' : show,

        # 'myFilter': myFilter,
    }
    return render(request,'main/view_bed_admin.html',context)    

def delete_bed(request,pk): 
        bed = Bed.objects.filter(id=pk)
        bed.delete()
        return redirect('/view_bed_admin/')


def update_bed(request,pk):
    user = request.user
    user_id = user.id
    pic = Profile.objects.filter(user=user_id)
    # print('----pic---',pics)
    show = False
    if pic.exists() :
        show = True
    print('------show------',show)
    bed = Bed.objects.get(id=pk)
    if request.method == "POST":
        bed_number = request.POST.get('bed_number')
        occupied = request.POST.get('occupied')
        categories = request.POST.get('categories')

        error = {} 

        if not validators.length(request.POST.get('bed_number'), min=2, max=100):
                error['bed_number'] = "Please enter bed number"

        if bool(error):
                return JsonResponse({'success': 'false', 'error': error}, status=422)  

        bed.bed_number = bed_number
        bed.occupied = occupied
        bed.categories = categories

        bed.save()
        return JsonResponse({'success': True, 'message': 'Successfully created', 'url': '/view_bed/'}, status=200)

    context = {
        'bed' : bed,
        'pic' : pic,
        'show' : show,

    }
    return render(request,'main/update_bed.html',context)

def update_bed_admin(request,pk):
    user = request.user
    user_id = user.id
    pic = Profile.objects.filter(user=user_id)
    # print('----pic---',pics)
    show = False
    if pic.exists() :
        show = True
    print('------show------',show)
    bed = Bed.objects.get(id=pk)
    if request.method == "POST":
        bed_number = request.POST.get('bed_number')
        occupied = request.POST.get('occupied')
        categories = request.POST.get('categories')
       
        error = {} 

        if not validators.length(request.POST.get('bed_number'), min=2, max=100):
                error['bed_number'] = "Please enter bed number"

        if bool(error):
                return JsonResponse({'success': 'false', 'error': error}, status=422)  

        bed.bed_number = bed_number
        bed.occupied = occupied
        bed.categories = categories
        bed.save()
        return JsonResponse({'success': True, 'message': 'Successfully created', 'url': '/view_bed/'}, status=200)

    context = {
        'bed' : bed,
        'pic' : pic,
        'show' : show,

    }
    return render(request,'main/update_bed_admin.html',context)

def add_user(request):
    curr_user = request.user
    user_id = curr_user.id
    pic = Profile.objects.filter(user=user_id)
    # print('----pic---',pics)
    show = False
    if pic.exists() :
        show = True
    print('------show------',show)
    if request.method == "POST":
        error = {}
        name = request.POST['name']
        phone_num = request.POST['phone_num']
        gender = request.POST['gender']
        dob = request.POST['dob']
        email = request.POST['email']
        address = request.POST['address']
        state = request.POST['state']
        city = request.POST['city']
        password = request.POST['password']
        
        if not validators.length(name, min=2, max=100):
                error['name'] = "Please enter valid name"
        if not validators.length(phone_num, max=10, min=10) or not phone_num.isdigit():
                error['phone_num'] = "Mobile number is not valid"
        if len(dob) == 0:
                error['dob'] = "Date is required"
        if not validators.email(email):
                error['email'] = "Please enter valid email"
        if len(address) == 0:
                error['address'] = "Address is required"
        if len(state) == 0:
                error['state'] = "State is required"
        if len(city) == 0:
                error['city'] = "City is required"

        if bool(error):
                return JsonResponse({'success': 'false', 'error': error}, status=422)

        characters = string.ascii_letters + string.digits
        password = "".join(choice(characters)for x in range(randint(8, 8)))

        profile_image_data = None
        if request.POST['profile_image'] != '' and request.POST['profile_image'] != None:
                profile_format, profile_img_data = request.POST['profile_image'].split(
                    ';base64,')
                ext = profile_format.split('/')[-1]
                profile_image_data = ContentFile(base64.b64decode(
                    profile_img_data), name='profile_image_'+str(int(time.time()))+'.'+ext)

        print('----date----',password)
        user = User.objects.create_user(
                username=name, password=password, email=email)
        create_user = Add_user.objects.create(
            user_id=user,
            name=name,
            phone_num=phone_num,
            gender=gender,
            dob=dob,
            email=email,
            address=address,
            state=state,
            city=city
        )
        print('userdata-------',create_user)
        create_user.save()

        add_profile = Profile.objects.create(
            user=user, 
        )
        if profile_image_data != None:
                add_profile.location = profile_image_data

        add_profile.save()

        un = 'Username: ' + name + '\n'
        pas = 'Password: ' + password + '\n'

        send = un + pas
        em = EmailMessage('Username and Password is send',send,'patelbhumin9177@gmail.com',to=[email])
        em.send()
        return JsonResponse({'success': True, 'message': 'Successfully created', 'url': '/user_list/'}, status=200)
        
    return render(request,'main/add_user.html',{'pic' : pic,'show' : show,})

def user_list(request):
    user = request.user
    user_id = user.id
    pic = Profile.objects.filter(user=user_id)
    # print('----pic---',pics)
    show = False
    if pic.exists() :
        show = True
    print('------show------',show)
    # user = Add_user.objects.all()
    # doctor_user = Doctor.objects.all()
    # users = list(chain(user,doctor_user))
    users = Add_user.objects.all()
    doctor = Doctor.objects.all()
    user = User.objects.all()

    context = {
        'users': users,
        'pic' : pic,
        'show' : show,
        # 'myFilter': myFilter
    }
    return render(request, 'main/user_list.html',context)

def delete_user(request,pk): 
        add_user = Add_user.objects.get(id=pk)
        user = User.objects.filter(id=add_user.user_id_id)
        user.delete()
        add_user.delete()
        print('------user-----',user)
        return redirect('/user_list/')

class user_master(ServerSideDatatableView):
    table_name = 'user_master'
    queryset = Add_user.objects.all()
    # doctor = Doctor.objects.all()
    # user = User.objects.all()
    # users = User.objects.filter(is_superuser = 0)  
    # print('------users------',users)
    # queryset = users.union(doctor)
    # queryset =  query.di.order_by('-name')[:10]
    # print('-----queryset----',queryset)
    columns = ['name','email','gender','id']

def user_update(request,pk):
    user = request.user
    user_id = user.id
    pic = Profile.objects.filter(user=user_id)
    # print('----pic---',pics)
    show = False
    if pic.exists() :
        show = True
    print('------show------',show)
    
    users = Add_user.objects.filter(pk=pk)
    show_doctor = False
    if users.exists():
        users = Add_user.objects.get(pk=pk)
        show_doctor = False
    else:
        users = Doctor.objects.get(pk=pk)
        show_doctor = True
    print('------',users)
    
    user_profile = Profile.objects.get(user=users.user_id)
    print('-----user_id-----',user_profile.location)
    if request.method == "POST":
        name = request.POST['name']
        phone_num = request.POST['phone_num']
        gender = request.POST['gender']
        dob = request.POST['dob']
        email = request.POST['email']
        address = request.POST['address']
        state = request.POST['state']
        city = request.POST['city']

        error = {}
        if not validators.length(request.POST['name'], min=2, max=100):
                error['name'] = "Please enter valid name"
        if not validators.length(request.POST['phone_num'], max=10, min=10) or not phone_num.isdigit():
                    error['phone_num'] = "Mobile number is not valid"
        if len(request.POST['dob']) == 0:
                    error['dob'] = "Date is required"
        if not validators.email(request.POST['email']):
                    error['email'] = "Please enter valid email"
        if len(request.POST['address']) == 0:
                    error['address'] = "Address is required"
        if len(request.POST['state']) == 0:
                    error['state'] = "State is required"
        if len(request.POST['city']) == 0:
                    error['city'] = "City is required"

        if bool(error):
                return JsonResponse({'success': 'false', 'error': error}, status=422)

        users.name=name
        users.phone_num=phone_num
        users.gender=gender
        users.dob = dob
        users.email=email 
        users.address=address
        users.state=state
        users.city=city
        users.save()

        characters = string.ascii_letters + string.digits

        user_update = User.objects.get(id=users.user_id_id)    
        user_update.username = name
        user_update.save()
        print('----------get-----',request.POST.get('email'))
        print('----------get-----',Add_user.email)

        if request.POST.get('email') != user_update.email:
            password = "".join(choice(characters)for x in range(randint(8, 8)))
            user_update.email = email        
            user_update.set_password(password)
            user_update.save()
            un = 'Username: ' + name + '\n'
            pas = 'Password: ' + password + '\n'

            send = un + pas
            em = EmailMessage('Username and Password is send',send,'patelbhumin9177@gmail.com',to=[email])
            em.send()

        profile_image_data = None
        if request.POST['profile_image'] != '' and request.POST['profile_image'] != None:
                profile_format, profile_img_data = request.POST['profile_image'].split(
                    ';base64,')
                ext = profile_format.split('/')[-1]
                profile_image_data = ContentFile(base64.b64decode(
                    profile_img_data), name='profile_image_'+str(int(time.time()))+'.'+ext)

                user_profile.location = profile_image_data
        user_profile.save()
        return JsonResponse({'success': True, 'message': 'Successfully created', 'url': '/user_list/'}, status=200)
    return render(request,'main/user_update.html',{'users':users,'pic' : pic,'user_profile':user_profile,'show' : show,'show_doctor' : show_doctor,})

def add_doctor(request):
    user = request.user
    user_id = user.id
    pic = Profile.objects.filter(user=user_id)
    # print('----pic---',pics)
    show = False
    if pic.exists() :
        show = True
    print('------show------',show)
    if request.method == "POST":
        error = {}
        name = request.POST['name']
        phone_num = request.POST['phone_num']
        gender = request.POST['gender']
        dob = request.POST['dob']
        email = request.POST['email']
        address = request.POST['address']
        state = request.POST['state']
        city = request.POST['city']
        specialization = request.POST['specialization']
        password = request.POST['password']
        
        if not validators.length(name, min=2, max=100):
                error['name'] = "Please enter valid name"
        if not validators.length(phone_num, max=10, min=10) or not phone_num.isdigit():
                error['phone_num'] = "Mobile number is not valid"
        if len(dob) == 0:
                error['dob'] = "Date is required"
        if not validators.email(email):
                error['email'] = "Please enter valid email"
        if len(address) == 0:
                error['address'] = "Address is required"
        if len(state) == 0:
                error['state'] = "State is required"
        if len(city) == 0:
                error['city'] = "City is required"
        if len(specialization) == 0:
                error['specialization'] = "Please enter specialization"

        if bool(error):
                return JsonResponse({'success': 'false', 'error': error}, status=422)
       
        characters = string.ascii_letters + string.digits
        password = "".join(choice(characters)for x in range(randint(8, 8)))

        profile_image_data = None
        if request.POST['profile_image'] != '' and request.POST['profile_image'] != None:
                profile_format, profile_img_data = request.POST['profile_image'].split(
                    ';base64,')
                ext = profile_format.split('/')[-1]
                profile_image_data = ContentFile(base64.b64decode(
                    profile_img_data), name='profile_image_'+str(int(time.time()))+'.'+ext)

        
        user = User.objects.create_user(
                username=name, password=password, email=email)
        create_doctor = Doctor.objects.create(
            user_id=user,
            name=name,
            phone_num=phone_num,
            gender=gender,
            dob=dob,
            email=email,
            address=address,
            state=state,
            city=city,
            specialization = specialization
        )
        print('userdata-------',create_doctor)
        create_doctor.save()

        add_profile = Profile.objects.create(
            user=user, 
        )
        if profile_image_data != None:
                add_profile.location = profile_image_data

        add_profile.save()

        un = 'Username: ' + name + '\n'
        pas = 'Password: ' + password + '\n'

        send = un + pas
        em = EmailMessage('Username and Password is send',send,'patelbhumin9177@gmail.com',to=[email])
        em.send()
        return JsonResponse({'success': True, 'message': 'Successfully created', 'url': '/user_list/'}, status=200)
        
    return render(request,'main/add_doctor.html',{'pic' : pic,'show' : show,})

def update_doctor(request,pk):
    user = request.user
    user_id = user.id
    pic = Profile.objects.filter(user=user_id)
    # print('----pic---',pics)
    show = False
    if pic.exists() :
        show = True
    print('------show------',show)
    doctor = Doctor.objects.get(pk=pk)
    # users=User.objects.all()
    user_profile = Profile.objects.get(user=doctor.user_id)
    print('--------------',doctor.specialization)
    if request.method == "POST":
        name = request.POST['name']
        phone_num = request.POST['phone_num']
        gender = request.POST['gender']
        dob = request.POST['dob']
        email = request.POST['email']
        address = request.POST['address']
        state = request.POST['state']
        city = request.POST['city']
        specialization = request.POST['specialization']

        error = {}
        if not validators.length(request.POST['name'], min=2, max=100):
                error['name'] = "Please enter valid name"
        if not validators.length(request.POST['phone_num'], max=10, min=10) or not phone_num.isdigit():
                    error['phone_num'] = "Mobile number is not valid"
        if len(request.POST['dob']) == 0:
                    error['dob'] = "Date is required"
        if not validators.email(request.POST['email']):
                    error['email'] = "Please enter valid email"
        if len(request.POST['address']) == 0:
                    error['address'] = "Address is required"
        if len(request.POST['state']) == 0:
                    error['state'] = "State is required"
        if len(request.POST['city']) == 0:
                    error['city'] = "City is required"
        if len(specialization) == 0:
                error['specialization'] = "Please enter specialization"
        if bool(error):
                return JsonResponse({'success': 'false', 'error': error}, status=422)

        doctor.name=name
        doctor.phone_num=phone_num
        doctor.gender=gender
        doctor.dob = dob
        doctor.email=email 
        doctor.address=address
        doctor.state=state
        doctor.city=city
        doctor.save()

        characters = string.ascii_letters + string.digits

        user_update = User.objects.get(id=doctor.user_id_id)    
        user_update.username = name
        user_update.save()
        print('----------get-----',request.POST.get('email'))
        print('----------get-----',Add_user.email)

        if request.POST.get('email') != user_update.email:
            password = "".join(choice(characters)for x in range(randint(8, 8)))
            user_update.email = email        
            user_update.set_password(password)
            user_update.save()
            un = 'Username: ' + name + '\n'
            pas = 'Password: ' + password + '\n'

            send = un + pas
            em = EmailMessage('Username and Password is send',send,'patelbhumin9177@gmail.com',to=[email])
            em.send()

        profile_image_data = None
        if request.POST['profile_image'] != '' and request.POST['profile_image'] != None:
                profile_format, profile_img_data = request.POST['profile_image'].split(
                    ';base64,')
                ext = profile_format.split('/')[-1]
                profile_image_data = ContentFile(base64.b64decode(
                    profile_img_data), name='profile_image_'+str(int(time.time()))+'.'+ext)

                user_profile.location = profile_image_data
        user_profile.save()

        return JsonResponse({'success': True, 'message': 'Successfully created', 'url': '/view_doctor/'}, status=200)
    return render(request,'main/update_doctor.html',{'doctor' : doctor,'pic' : pic,'show' : show,'user_profile': user_profile})

def view_doctor(request):
    user = request.user
    user_id = user.id
    pic = Profile.objects.filter(user=user_id)
    # print('----pic---',pics)
    show = False
    if pic.exists() :
        show = True
    print('------show------',show)
    doctor = Doctor.objects.all()

    # filtering
#     myFilter = DoctorFilter(request.GET, queryset=doctor)

#     doctor = myFilter.qs
    context = {
        'doctor': doctor,
        'pic' : pic,
        'show' : show,
        # 'myFilter': myFilter
    }
    return render(request,'main/view_doctor.html',context)

def delete_doctor(request,pk): 
        doctor = Doctor.objects.get(id=pk)
        user = User.objects.filter(id=doctor.user_id_id)
        user.delete()
        doctor.delete()
        print('------user-----',user)
        return redirect('/view_doctor/')

class doctor_master(ServerSideDatatableView):
    table_name = 'doctor_master'
    queryset = Doctor.objects.all()
    columns = ['name', 'phone_num','specialization','id']

def autobed(request):
    query_original = request.GET.get('term')
    queryset = Bed.objects.filter(name__icontains=query_original)
    mylist = []
    mylist += [x.name for x in queryset]
    return JsonResponse(mylist, safe=False)

def edit_profile(request):
    user = request.user
    user_id = user.id
    pic = Profile.objects.filter(user=user_id)
    # print('----pic---',pics)
    show = False
    if pic.exists() :
        show = True
    print('------show------',show)
    users = Add_user.objects.get(user_id=user_id)
    print('-----',users)
    if request.method == "POST":
        name = request.POST['name']
        phone_num = request.POST['phone_num']
        gender = request.POST['gender']
        dob = request.POST['dob']
        email = request.POST['email']
        address = request.POST['address']
        state = request.POST['state']
        city = request.POST['city']

        error = {}
        if not validators.length(request.POST['name'], min=2, max=100):
                error['name'] = "Please enter valid name"
        if not validators.length(request.POST['phone_num'], max=10, min=10) or not phone_num.isdigit():
                    error['phone_num'] = "Mobile number is not valid"
        if len(request.POST['dob']) == 0:
                    error['dob'] = "Date is required"
        if not validators.email(request.POST['email']):
                    error['email'] = "Please enter valid email"
        if len(request.POST['address']) == 0:
                    error['address'] = "Address is required"
        if len(request.POST['state']) == 0:
                    error['state'] = "State is required"
        if len(request.POST['city']) == 0:
                    error['city'] = "City is required"

        if bool(error):
                return JsonResponse({'success': 'false', 'error': error}, status=422)

        users.name=name
        users.phone_num=phone_num
        users.gender=gender
        users.dob = dob
        users.email=email 
        users.address=address
        users.state=state
        users.city=city
        users.save()

        user_update = User.objects.get(id=users.user_id_id)
        user_update.email = email            
        user_update.username = name
        user_update.save()

        profile = Profile.objects.get(user_id=user_id)
        print('------pic----',profile)

        profile_image_data = None
        if request.POST['profile_image'] != '' and request.POST['profile_image'] != None:
                profile_format, profile_img_data = request.POST['profile_image'].split(
                    ';base64,')
                ext = profile_format.split('/')[-1]
                profile_image_data = ContentFile(base64.b64decode(
                    profile_img_data), name='profile_image_'+str(int(time.time()))+'.'+ext)
                profile.location = profile_image_data
        profile.save()

    context = {
        'users' : users,
        'pic' : pic,
        'show' : show,
    }
    return render(request,'main/edit_profile.html',context)

def edit_profile_admin(request):
    user = request.user
    user_id = user.id
    users = User.objects.get(id=user_id)
    users_id = users.id
    pic = Profile.objects.filter(user=users_id)
    # pics = pic.values_list('location', flat = True)
    # print('----pic---',pics)
    show = False
    if pic.exists() :
        show = True
    print('------show------',show)
    print('-----',user_id)
    if request.method == "POST":
        name = request.POST['name']
        fname = request.POST['first_name']
        lname = request.POST['last_name']
        email = request.POST['email']
        

        error = {}
        if not validators.length(request.POST['name'], min=2, max=100):
                error['name'] = "Please enter valid name"

        if bool(error):
                return JsonResponse({'success': 'false', 'error': error}, status=422)

        user.username=name
        user.first_name=fname
        user.last_name=lname
        user.email=email
        user.save()

        profile_get = Profile.objects.filter(user_id=user_id)
        if profile_get.exists():
            profile = Profile.objects.get(user_id=user_id)

            profile_image_data = None
            if request.POST['profile_image'] != '' and request.POST['profile_image'] != None:
                    profile_format, profile_img_data = request.POST['profile_image'].split(
                        ';base64,')
                    ext = profile_format.split('/')[-1]
                    profile_image_data = ContentFile(base64.b64decode(
                        profile_img_data), name='profile_image_'+str(int(time.time()))+'.'+ext)
                    profile.location = profile_image_data
            profile.save()
        else:
            create_profile=Profile.objects.create(
                user_id = users_id
            )
            create_profile.save()
            profile = Profile.objects.get(user_id=user_id)

            profile_image_data = None
            if request.POST['profile_image'] != '' and request.POST['profile_image'] != None:
                    profile_format, profile_img_data = request.POST['profile_image'].split(
                        ';base64,')
                    ext = profile_format.split('/')[-1]
                    profile_image_data = ContentFile(base64.b64decode(
                        profile_img_data), name='profile_image_'+str(int(time.time()))+'.'+ext)
                    profile.location = profile_image_data
            profile.save()

    context = {
        'user' : user,
        'pic' : pic,
        'show' : show,
    }
    return render(request,'main/edit_profile_admin.html',context)

def view_ur_patient(request):
    user = request.user
    user_id = user.id
    users = User.objects.get(id=user_id)
    users_id = users.id
    pic = Profile.objects.filter(user=users_id)
    # pics = pic.values_list('location', flat = True)
    # print('----pic---',pics)
    show = False
    if pic.exists() :
        show = True
    print('------show------',show)
    patient = Patient.objects.all()
    doctor = Doctor.objects.get(user_id=user.id)
    patients = Patient.objects.filter(doctor=doctor)
    print('------doc----',doctor)
    print('------doc----',patients)

    context = {
        'pic' : pic,
        'show' : show,
        'patients' : patients,
    }
    return render(request,'main/view_ur_patient.html',context)


class doctor_patient_master(ServerSideDatatableView):
    table_name = 'patient_doctor_master'
    # def get_user(self,request):
    #     user = request.user
    #     user_id = user.id
    #     users = User.objects.filter(id=user_id)
    #     return users
    # doctor = Doctor.objects.filter(user_id=get_user)
    # queryset = Patient.objects.all()
    def get(self, request, *args, **kwargs):
        user_id = request.user.id
        print('user_id:',user_id)
        doctor = Doctor.objects.get(user_id=user_id)
        self.queryset = Patient.objects.filter(doctor=doctor)
        return super(doctor_patient_master, self).get(request, *args, **kwargs)
    columns = ['name', 'bed_num__bed_number', 'status','id']
    


def edit_profile_doctor(request):
    user = request.user
    user_id = user.id
    pic = Profile.objects.filter(user=user_id)
    # print('----pic---',pics)
    show = False
    if pic.exists() :
        show = True
    print('------show------',show)
    users = Doctor.objects.get(user_id=user_id)
    print('-----',users)
    if request.method == "POST":
        name = request.POST['name']
        phone_num = request.POST['phone_num']
        gender = request.POST['gender']
        dob = request.POST['dob']
        email = request.POST['email']
        address = request.POST['address']
        state = request.POST['state']
        city = request.POST['city']

        error = {}
        if not validators.length(request.POST['name'], min=2, max=100):
                error['name'] = "Please enter valid name"
        if not validators.length(request.POST['phone_num'], max=10, min=10) or not phone_num.isdigit():
                    error['phone_num'] = "Mobile number is not valid"
        if len(request.POST['dob']) == 0:
                    error['dob'] = "Date is required"
        if not validators.email(request.POST['email']):
                    error['email'] = "Please enter valid email"
        if len(request.POST['address']) == 0:
                    error['address'] = "Address is required"
        if len(request.POST['state']) == 0:
                    error['state'] = "State is required"
        if len(request.POST['city']) == 0:
                    error['city'] = "City is required"

        if bool(error):
                return JsonResponse({'success': 'false', 'error': error}, status=422)

        users.name=name
        users.phone_num=phone_num
        users.gender=gender
        users.dob = dob
        users.email=email 
        users.address=address
        users.state=state
        users.city=city
        users.save()

        user_update = User.objects.get(id=users.user_id_id)
        user_update.email = email            
        user_update.username = name
        user_update.save()

        profile = Profile.objects.get(user_id=user_id)
        print('------pic----',profile)

        profile_image_data = None
        if request.POST['profile_image'] != '' and request.POST['profile_image'] != None:
                profile_format, profile_img_data = request.POST['profile_image'].split(
                    ';base64,')
                ext = profile_format.split('/')[-1]
                profile_image_data = ContentFile(base64.b64decode(
                    profile_img_data), name='profile_image_'+str(int(time.time()))+'.'+ext)
                profile.location = profile_image_data
        profile.save()

    context = {
        'users' : users,
        'pic' : pic,
        'show' : show,
    }

    return render(request,'main/edit_profile_doctor.html',context)

def doctor_edit_patient(request,pk):
    user = request.user
    user_id = user.id
    pic = Profile.objects.filter(user=user_id)
    # print('----pic---',pics)
    show = False
    if pic.exists() :
        show = True
    print('------show------',show)
    beds = Bed.objects.filter(occupied='No')
    patient = Patient.objects.get(id=pk)
    doctors = Doctor.objects.all()
    old = patient.bed_num_id

    if request.method == "POST":
        
        name = request.POST['name']
        mobile = request.POST['phone_num']
        mobile2 = request.POST['mobile2']
        relativeName = request.POST['relativeName']
        gender = request.POST['gender']
        address  = request.POST['location']
        prior_ailments = request.POST['prior_ailments']
        status = request.POST['status']
        bed_num_sent = request.POST['bed_num']
        bed_num = Bed.objects.get(bed_number=bed_num_sent)
        dob = request.POST['dob']
        doctor = request.POST['doctor']
        doctor = Doctor.objects.get(name=doctor)
        comman_symptoms = request.POST.getlist('symptoms')
        less_symptoms = request.POST.getlist('symptoms2')
        serious_symptoms = request.POST.getlist('symptoms3')

        error = {}
        if not validators.length(request.POST['name'], min=2, max=100):
                error['name'] = "Please enter valid name"
        if not validators.length(mobile, max=10, min=10) or not mobile.isdigit():
                error['phone_num'] = "Mobile number is not valid"
        if not validators.length(relativeName, min=2, max=100):
                error['relativeName'] = "Please enter relative name"
        if not validators.length(mobile2, max=10, min=10) or not mobile2.isdigit():
                error['mobile2'] = "Mobile number is not valid"
        if len(address) == 0:
                error['location'] = "Address is required"
        if not validators.length(prior_ailments, min=2, max=100):
                error['prior_ailments'] = "Please enter prior ailments"
        if len(request.POST['bed_num']) == 0:
                error['bed_num'] = "Bed number is required"
        if len(dob) == 0:
                error['dob'] = "Date is required"
        if len(status) == 0:
                error['status'] = "Status is required"
        if len(request.POST['doctor']) == 0:
                error['doctor'] = "Doctor is required"

        if bool(error):
                return JsonResponse({'success': 'false', 'error': error}, status=422)        

        patient.name = name
        patient.bed_num = bed_num
        patient.phone_num = mobile
        patient.patient_relative_contact = mobile2
        patient.patient_relative_name = relativeName
        patient.gender=gender
        patient.address = address
        patient.prior_ailments = prior_ailments
        patient.dob = dob
        patient.doctor = doctor
        patient.status = status
        patient.comman_symptoms = comman_symptoms
        patient.less_symptoms = less_symptoms
        patient.serious_symptoms = serious_symptoms
        print('----------------bed_num--------------',patient.bed_num)
        patient.save()
        print('----------------bed_num--------------',patient.bed_num)

        bed = Bed.objects.get(id=patient.bed_num_id)
        print('----------------bed_num--------------',bed)
        bed.occupied = 'Yes' 
        if patient.status == 'Recoverd':
            bed.occupied = 'No'
        bed.save()
        beds = Bed.objects.get(id=old)
        print('----------------bed_numsssss--------------',beds)
        beds.occupied = 'No' 
        beds.save()
        id = patient.id
        # return redirect(f"/patient/{id}")
        return JsonResponse({'success': True, 'message': 'Successfully created', 'url': '/patient_list/'}, status=200)

    context = {
        'patient': patient,
        'beds' : beds,
        'doctors' : doctors,
        'pic' : pic,
        'show' : show,

    }
    return render(request,'main/doctor_edit_patient.html',context)
